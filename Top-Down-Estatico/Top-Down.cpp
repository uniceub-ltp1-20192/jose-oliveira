#include <iostream>
#include <conio.h>
// #include <ncurses.h>
#include <string>
#include <ctype.h>
#include <iomanip>
#include <ctime>
#include <algorithm>

using namespace std;


//prot�tipo da fun��o scanName
string scanName();

//prot�tipo da fun��o to upper
string toUpperCase(string name);

//prot�tipo da fun��o validateName
bool validateName(string name);

//prot�tipo da fun��o valideVogalsCapitalLetter
bool validadeVogalsCapitalLetter(string name);

//prot�tipo da fun��o getGender
string getGender(string name);

//prot�tipo da fun��o getWeaponInName
string getWeaponInName(string name);

//prot�tipo da fun��o isWomanAssassin
bool isWomanAssassin(string gender, string weapon);

//prot�tipo da fun��o isManAssassin
bool isManAssassin(string gender, int hour, int minute);

int main()
{

	//Utilizando charset para l�ngua portuguesa
	setlocale(LC_ALL, "PORTUGUESE");

	//fun��o para pedir ao usu�rio o nome do suspeito
	string  name = scanName();
	
	//Colocando nome para upper case
	name = toUpperCase(name);

	//Estrutura para Retornar do Sistema a hora
	struct tm *local;
	time_t t;
	t = time(NULL);
	local = localtime(&t);

	int hour = local->tm_hour;
	int minute = local->tm_min;

	////Pegando a valida��o do nome 
	bool  Validname = validateName(name);

	//Caso a validacao tenha retornado falsa
	if (!Validname) {
		cout << "Nome invalido ou vazio, voce deve escrever um nome valido\n";
		// system("read b");
		_getch();
		return 0;
	}

	//Pegando a validacao das vogais
	bool validVogalsName = validadeVogalsCapitalLetter(name);

	//Caso a Valida��o das vogais tenha retornado nulo
	if (!validVogalsName) {
		cout << "A pessoa identificada no nome nao pode ser suspeita\n";
		// system("read b");
		_getch();
		return 0;
	}

	//Recuperando g�nero
	string gender = getGender(name);

	//Recuperando a arma
	string weapon = getWeaponInName(name);

	//Recuperando o valor false ou true caso o assassino seja mulher e a condi��o da arma branca sejam ambas satisfeitas
	bool validAW = isWomanAssassin(gender, weapon);

	//Recuperando o valor false ou true caso o assassino seja homem e estaja no sagu�o as 00h30
	bool validAM = isManAssassin(gender, hour, minute);

	//Verificando para ver se alguma das valida��es retornou verdadeiro    
	if (!validAM && !validAW) {
		cout << "O Suspeito nao eh o ASSASSINOO\n";
		// system("read b");
		_getch();
		return 0;
	}

	//Caso uma das valida��es retorne verdadeiro, ser� mostrado uma menssagem 
	cout << "ELE(A) EEH O(A) ASSASSINOOOO(AAAAAA)!!!!\n";
	// system("read b");
	_getch();
	return 0;
}


//Fun��o scanName para pegar o nome digitado pelo usu�rio
string scanName() {

	//Vari�vel que ir� guardar o nome
	string name;

	//Dar uma mensagem de boas vindas
	cout << "Bem-vindo(a) ao hotel morte lenta\n";
	cout << "Digite o nome para iniciar a investigacao: ";
	getline(cin, name);

	return name;
}

string toUpperCase(string name) {
	for (int i = 0; i < name.size(); i++) {
		name[i] = toupper(name[i]);
	}
	
	return name;
}

//Fun��o para validar nome passado
bool validateName(string name) {

	/*toupper(name);*/

	//Caracteres de express�es irregulares
	string invalidExpression = "!@#$%&*()_-=+1234567890{[}]:;?/*\\";

	//Verificando a exist�mcia de espa�os duplicados
	for (int i = 0; i < name.size(); i++) {			
		if (name[i] == ' ' && (name[i + 1] == ' ' || !name[i + 1])) {
			return false;
		}
		//N�o pode haver espa�os ap�s a ultima letra
		if(name[name.size()] == ' '){
			return false;
		}
	}  
	
	//Verificando a exist�ncia de caracteres inv�lidos
	if (name.find_first_of(invalidExpression) != std::string::npos){
		return false;
	}

	return true;
}

//Fun��o para verificar se tem = ou  mais de tr�s vogais e nenhuma � O ou U
bool validadeVogalsCapitalLetter(string name) {

	//Express�o que ir� conter as vogais n�o aceitas
	string expressionInvalidVogals = "OU";

	//Verificando quantas vezes aparecem as vogais v�lidas
	size_t n1 = count(name.begin(), name.end(), 'A');
	size_t n2 = count(name.begin(), name.end(), 'E');
	size_t n3 = count(name.begin(), name.end(), 'I');
	
	//Validando quantas vezes as vogais v�lidas aparecem
	if((n1 + n2 + n3) < 3) {
		return false;
	}
	
	//Validando se uma vogal inv�lida aparece
	if (name.find_first_of(expressionInvalidVogals) != std::string::npos){
		return false;
	}
	
	return true;
}

//Fun��o para recuperar g�nero do suspeito
string getGender(string name) {

	//booleano para verifica��o da exist�ncia de um sobrenome
	bool hasSecondName = false;

	//String que conter� o g�nero
	string gender;

	//String para recuperar o primeiro nome
	string firstName = "";

	//Caractere para recuperar a �ltima letra do primeiro nome;
	char lastLetter;

	//Breakpoint de identifica��o de espa�o para autentica��o de um sobrenome
	char breakPoint = ' ';

	//Verifica��o da existencia de um sobrenome
	int n = name.find(breakPoint);

	//Caso n�o tenha um sobrenome
	if (n == -1) {
		firstName = name;
	}

	//Caso tenha um sobrenome
	if (n != -1) {
		for (int i = 0; i < name.size(); i++) {

			if (name[i] == breakPoint) {
				firstName += " ";
				break;
			}
			firstName += name[i];
		}
	}
	
	//Recuperando a ultima letra do primeiro nome
	for (int i = 0; i < firstName.size(); i++) {
		if (firstName[i] == breakPoint) {
			lastLetter = firstName[i - 1];
		}
	}

	//verificando se � homem
	if (lastLetter != 'A') {
		gender = "male";
	}

	//Verificando se � mulher
	if (lastLetter == 'A') {
		gender = "female";
	}

	return gender;
}

//Fun��o para verificar a arma utilizada pelo usu�rio
string getWeaponInName(string name) {

	//Breakpoint de identifica��o de espa�o para autentica��o de um sobrenome
	char breakPoint = ' ';

	//booleano para verifica��o da exist�ncia de um sobrenome
	bool hasSecondName = false;
	
	//Vari�vel que ir� guardar arma(sobrenome) do suspeito
	string weapon = "";
	
	//Verificando a exist�ncia de um sobrenome
	int n = name.find(breakPoint);

	//Caso n�o tenha sobrenome
	if (n == -1) {
		return "notWeapon";
	}
	
	//Recuperando arma(sobrenome) do suspeito
	weapon = name.substr(n+1);

	return weapon;
}

//Fun��o para verificar se o assassino � mulher
bool isWomanAssassin(string gender, string weapon) {

	//Caso o g�nero n�o seja feminino
	if (gender != "female") {
		return false;
	}

	//Caso n�o tenha arma
	if (weapon == "notWeapon") {
		return false;
	}
	
	//Vetor de todas as armas de fogo que consegui lembrar
	string notWhiteWeapons [8] = { "PISTOLA", "REVOLVER", "CANHAO", "FUZIL", "METRALHADORA", "ESCOPETA", "RIFLE", "SUB-METRALHADORA" };

	//Verificando se a arma do suspeito � uma arma de fogo
	for (int i = 0; i < 8; i++) {

		if (weapon == notWhiteWeapons[i]) {
			return false;
		}
	}

	return true;
}

//Fun��o para verificar se o assassino � homem
bool isManAssassin(string gender, int hour, int minute) {

	//Se o g�nero for diferente de masculino
	if (gender != "male") {
		return false;
	}

	//Se a hora for diferente de 00h30
	if (hour != 0 || minute != 30) {
		return false;
	}

	return true;
}